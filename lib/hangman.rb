class Hangman
  @guesser
  @referee
  @board

  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    options = {
      guesser: HumanPlayer.new("Oedipus"),
      referee: ComputerPlayer.new([])
    }.merge(options)

    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    correct_guesses = @referee.check_guess(@guesser.guess)
    update_board
    @guesser.handle_response
  end

  def update_board
    # update with correct_guesses output here
  end

end

class HumanPlayer
  @name
  @secret_length

  def initialize(name)
    @name = name
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def guess
    gets.chomp.downcase
  end

  def handle_response(*args)
  end
end

class ComputerPlayer
  @dictionary
  @secret_word
  @secret_length

  attr_reader :dictionary, :secret_word

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.chars.each_with_index do |ch, index|
      indices.push(index) if ch == letter
    end
    indices
  end

  def register_secret_length(length)
    @secret_length = length
    @dictionary = @dictionary.select { |word| word.length == @secret_length }
  end

  def guess(board)
    count = Hash.new(0)
    puts @dictionary.join
    @dictionary.join.each_char do |ch|
      count[ch] += 1
    end
    # If no correct guesses have been made
    if board.all? { |letter| letter == nil }
      count.sort_by { |key, value| value }.last[0]
    # If some guesses have been made
    else
      nth_last = -2
      loop do
        if board.include?(count.sort_by { |key, value| value }[nth_last][0])
          nth_last -= 1
          next
        else
          return count.sort_by { |key, value| value }[nth_last][0]
        end
      end
    end
    # end
    # following code to be removed once top code is done
    # alphabet = *('a'..'z')
    # alphabet.sample
  end

  def handle_response(*args)
    # if secret word does not contain guessed letter
    if args[1].empty?
    @dictionary.reject! { |word| word.include?(args[0]) }
    # if secret word contains guessed letter
    else
      cuts_candidates(*args)
    end
  end

  def cuts_candidates(*args)
    temp_dict = []
    # loop through dictionary
    @dictionary.each do |word|
      # count used for correct letter and correct index
      match_count = 0
      args[1].each do |index|
        match_count += 1 if word[index] == args[0]
      end
      # count used for correct letter but both incorrect and correct indices
      total_count = 0
      word.each_char do |ch|
        total_count += 1 if ch == args[0]
      end
      # add word if it matches guess specifications
      if match_count == args[1].length && total_count == args[1].length
        temp_dict.push(word)
      end
    end
    # return new candidate words
    @dictionary = temp_dict
  end


  def candidate_words
    @dictionary
  end
end
